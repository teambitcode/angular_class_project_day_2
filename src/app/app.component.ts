import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'AngularDay2';
  myValue = true;


    constructor(public routerVar: Router){

    }

  getValue(value){
    console.log("Emited ***********");
    console.log(value);
  }

  navigateToTable(){
    this.routerVar.navigateByUrl('table');
  }

  navigateToProducts(){
    this.routerVar.navigateByUrl('all-products');
  }
  navigateToUsers(){
    this.routerVar.navigateByUrl('all-users');
  }
}
