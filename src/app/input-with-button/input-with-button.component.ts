import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-input-with-button',
  templateUrl: './input-with-button.component.html',
  styleUrls: ['./input-with-button.component.scss']
})
export class InputWithButtonComponent implements OnInit, OnChanges {
  changeValue: string = "";
  @Output() tableOutputEvent = new EventEmitter<any>();

  @Input()  isVisibleClickButton = false;
////////

  @Input() isVisibleClickButtonOne1 = null;
  @Input() isVisibleClickButtonSecond2 = false;
  @Input() isVisibleClickButtonThird3 = false;


  constructor() {
    // console.log("InputWithButtonComponent constructor running");
  }

  ngOnChanges(changes: SimpleChanges): void {
    
    if(changes.isVisibleClickButtonOne1){
      console.log('isVisibleClickButtonOne1 *****');
      console.log(changes.isVisibleClickButtonOne1);
    }



    if(changes.isVisibleClickButtonSecond2){
      console.log('isVisibleClickButtonSecond2 *****');
    }
    // if(changes.isVisibleClickButtonThird3){
    //   console.log('isVisibleClickButtonThird3 *****');
    // }
  }

  ngOnInit(): void {
    console.log("InputWithButtonComponent ngOnInit running");
  }
  

  buttonEvent(){
    console.log("clicked********");
    this.tableOutputEvent.emit(this.changeValue);
  }
  getTypeValue(value){
    console.log(value.target.value);
    this.tableOutputEvent.emit(value.target.value);
  }
}
