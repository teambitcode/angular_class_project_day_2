import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MainDataService } from '../main-data.service';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users: any[] = [];
  userEmail:string = '';
  constructor(private mainDataService: MainDataService) { }

  ngOnInit(): void {
    this.getAllUsers();
  }
  getAllUsers() {

    this.mainDataService.callGetApi('/user/getAll').subscribe((response: ResponseData) => {
      console.log(response);
      if (response.data) {
        this.users = response.data;
      }
    });



    // this.http.get('http://68.183.109.214:3000/user/getAll').subscribe((response: ResponseData) => {
    //   console.log(response);
    //   if (response.data) {
    //     this.users = response.data;
    //   }
    // });
  }

  createUser(){
    let tempBody = {
      "role":"user",
      "email": this.userEmail,
      "password":"1234abcd",
      "first_name":"MY first name",
      "last_name":"MY last name",
    };

    this.mainDataService.callPostApi('/user/new',tempBody).subscribe((response:ResponseData)=>{
      if(response.status){
        console.log("User created....");
        this.getAllUsers();
      }
      });


    // this.http.post('http://68.183.109.214:3000/user/new',tempBody).subscribe((response:ResponseData)=>{
    // if(response.status){
    //   console.log("User created....");
    //   this.getAllUsers();
    // }
    // });
  }
}

export class ResponseData {
  status: boolean;
  data: any;
}
