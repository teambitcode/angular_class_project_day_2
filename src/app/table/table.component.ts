import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { MainDataService } from '../main-data.service'
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  @Input() isHomePage = false;

  @Output() tableOutputEvent = new EventEmitter<any>();

  tableName: string = "My Student Data";
  age: number = 8;
  changeValue: string = "";


  data1  = {age:0};
  
    data2 = true;
  data3 = true;

  constructor(private mainDataService : MainDataService) {
    // console.log("TableComponent constructor running");
  }
  studentData: Student[] = [
    {
      name: "SSSSS",
      subject: "IT",
      marks: 90
    },
    {
      name: "BBBBB",
      subject: "English",
      marks: 80
    },
    {
      name: "LLLLL",
      subject: "IT",
      marks: 20
    },
    {
      name: "GGGGGG",
      subject: "English",
      marks: 30
    },
    {
      name: "KKKKK",
      subject: "Maths",
      marks: 50
    }]

  ngOnInit(): void {  
    this.tableName = this.mainDataService.getMyTitle();
    // console.log("TableComponent ngOnInit running");
  }

  loadData() {
    console.log("Loding data ...**");
  }

  buttonEvent() {
    this.tableOutputEvent.emit(this.studentData);
  }

  getOutputFronChildInput(eventValue){
    console.log(eventValue);
    this.changeValue = eventValue;
  }
  getAge() {
    return 20;
  }
  getStudentDataArray(){
    let myArray = [];
    for (let index = 0; index < this.studentData.length; index++) {
      const element = this.studentData[index];
      if(element.subject.toLocaleLowerCase().indexOf(this.changeValue.toLocaleLowerCase()) !== -1 ){
        myArray.push(element);
      }
    }
   return myArray;

  }





  buttonEvent1() {
    this.data1.age = 11;
  }
  buttonEvent2() {
    this.data2 = !this.data2;
  }
  buttonEvent3() {
    this.data3 = !this.data3;
  }


  setData(){
    // this.tableName = "New Table Name with click";
    this.mainDataService.setMyTitle("New Table Name with click");
    this.tableName = this.mainDataService.getMyTitle();
    
  }
  printTableName(){
    console.log(this.tableName);
  }
}


class Student {
  name: string;
  subject: string;
  marks: number;
}
