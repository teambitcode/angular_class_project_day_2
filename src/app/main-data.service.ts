import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root',
})
export class MainDataService {
    private myTitle: string = '';

    constructor(private http: HttpClient){

    }

    setMyTitle(title: string) {
        this.myTitle = title;
    }
    getMyTitle() {
        return this.myTitle;
    }

    calculate(firstNumber:number, seconfNumber:number){
            return firstNumber + seconfNumber;
    }

    callGetApi(url: string): Observable<any> {
        
        return this.http.get(environment.basePath+url);
    }

    
    callPostApi(url: string, body:any): Observable<any> {
        return this.http.post(environment.basePath+url,body);
    }

}