import { Component, OnInit } from '@angular/core';
import { MainDataService } from '../main-data.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
lastTitle: string = '';
  constructor(public mainDataService : MainDataService) { }

  ngOnInit(): void {
    this.lastTitle = this.mainDataService.getMyTitle();
  }

}
